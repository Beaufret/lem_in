/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ant_moves_rounds.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/03 17:21:46 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/12/03 17:22:47 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	ft_is_more_to_launch_this_cycle(int *ants_start, int i, int j)
{
	int res;

	res = 0;
	while (++i < j)
	{
		//ft_printf("{YELLOW}ANTS_START[%i]{EOC} vaut %i \n", i, ants_start[i]);
		if (ants_start[i] > 0)
			res = res + 1;
	}	
	return (res);
}

void		ft_print_first_round(int *ants_start,
		t_list **ants, t_params **params, t_list **bib, int *ants_end)
{
	int		i;

	i = 0;
	while (i < (*params)->index)
	{
		if (ants_start[i] > 0)
		{
			if ((*params)->allow_printing_moves == 1)
			{
				ft_printf("L%i-%s", (*params)->ants_launched + 1,
						((t_node *)bib[i]->next->content)->name);
				if (ft_is_more_to_launch_this_cycle(ants_start, i,
					(*params)->index) != 0)
					ft_printf(" ");
			}
			ants[(*params)->ants_launched] = bib[i]->next;
			if (ants[(*params)->ants_launched]->next == NULL)
			{
				ants[(*params)->ants_launched] = NULL;
				(*ants_end)++;
			}
			(*params)->ants_launched++;
			ants_start[i]--;
		}
		i++;
	}
	if ((*params)->allow_printing_moves == 1)
		ft_printf("\n");
}

static int	ft_is_more_to_move(t_list **ants, t_params **params, int i)
{
	int res;

	res = 0;
	while (++i < (*params)->ant_count)
	{
		if (ants[i] != NULL)
			res++;	
	}
	return (res);
}

void		ft_print_other_rounds(int *ants_end, t_list **ants,
		t_params **params, int *ants_start)
{
	int		j;

	j = 0;
	while (j < (*params)->ant_count)
	{
		if (ants[j] != NULL && (ants[j]->next != NULL || (*params)->rooms_count == 2))
		{
			if ((*params)->allow_printing_moves == 1 && (*params)->rooms_count != 2)
			{
				ft_printf("L%i-%s", j + 1,
						((t_node *)ants[j]->next->content)->name);
				if (ft_is_more_to_launch_this_cycle(ants_start, -1,
						(*params)->index) != 0 ||
						ft_is_more_to_move(ants, params, j) != 0)
				ft_printf(" ");
			}
			ants[j] = (ants[j]->next != NULL) ? ants[j]->next : ants[j];
			if (ants[j]->next == NULL)
			{
				ants[j] = NULL;
				(*ants_end)++;
			}
		}
		j++;
	}
}
